import requests
from bs4 import BeautifulSoup as bs
from random import choice


def get_proxy():
    p_ip_list = [
        'https://91.206.210.54:46698',
        'https://46.172.76.114:49472',
        'https://197.232.69.137:51365',
        'https://212.28.237.130:32529',
        'https://86.57.219.183:23500',
        'https://87.197.137.223:55622',
        'https://112.78.187.186:51038',
    ]
    selected = choice(p_ip_list)
    proxies = {protocol: selected for protocol in ('https', 'http')}
    return proxies


def proxy_request(request_type, url, **kwargs):
    proxy = get_proxy()
    print(proxy)
    r = requests.request(request_type, url, timeout=5, **kwargs)
    return r


ree = proxy_request('get', 'https://www.airbnb.com/s/Baku--Azerbaijan/homes?refinement_paths%5B%5D=%2Fhomes&federated_search_session_id=f5764880-c40d-49ba-a722-540f6add1559&query=Baku%2C%20Azerbaijan&place_id=ChIJ-Rwh1mt9MEARa2zlel5rPzQ&search_type=autocomplete_click')
print(ree.text)
